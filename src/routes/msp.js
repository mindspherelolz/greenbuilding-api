import express from 'express';
import config from '../config';
import middleware from '../middleware';
import initalizeDb from '../db';
import events from '../controller/msp/events';
import assets from '../controller/msp/assets';
import notification from '../controller/msp/notification';
import timeseries from '../controller/msp/timeseries'

let router = express();
//MindSphere api
//assets (/msp/assets)
router.use('/assets', assets);
//events (/msp/events)
router.use('/events', events);
//timeseries (/msp/timeseries)
router.use('/timeseries', timeseries);

router.use('/notification', notification);


export default router;
