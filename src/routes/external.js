import express from 'express';
import meteorological from '../controller/external/meteorological'

let router = express();
//external api
//meteorological (/external/meteorological)
router.use('/meteor', meteorological);


export default router;
