import Message from '../model/message'
module.exports =  (io) => {
        //Listen for connection
        let notiSocket = io.of('/noti');
        notiSocket.on('connection', function(client){
        console.log('a user connected');
        //Listens for a new message
        client.on('newMessage', function(priority, subject, body) {
            //Create Message
            let newMessage = new Message({
            priority: priority,
            subject: subject,
            body: body
        });
            //Save it to database
            newMessage.save(function(err, message){
            //Send message to those connected in the noti
            console.log('new message created');
            notiSocket.emit("messageCreated", message);
            });
        });

        //Listens for a message deleted
        client.on('deleteMessageById', function(Id) {
            //Delete Msg from database
            Message.remove({
            _id: Id
            }, (err, message) => {
            if (err) {
                console.log(err);
            }
            console.log('A message deleted by id');
            notiSocket.emit("aMessageDeleteById", message)
            });
        });

        //Listens for a message read
        client.on('readMessage', function(Id) {
            //Read Msg and save status to database
            Message.findOneAndUpdate({ _id: Id }, {"read": true}, (err, message) => {
            if (err) {
                console.log(err);
            }
            console.log('A message read by an user');
            notiSocket.emit("aMessageRead", message)
        });
        });

        //Listens for a message solved
        client.on('solvedMessage', function(Id) {
            //Solved Msg and save status to database
            Message.findOneAndUpdate({ _id: Id }, {"solved": true}, (err, message) => {
            if (err) {
                console.log(err);
            }
            console.log('A message solved by an user');
            notiSocket.emit("aMessageSolved", message)
        });
        });
        });
};