import { Router } from 'express'
import User from '../model/user';

export default({ config, db}) => {
    let api = Router();

    // '/v1/user/add' - Create
    api.post('/add', (req,res) => {
        let newUser = new User();
        newUser.cardId = req.body.cardId;
        newUser.name = req.body.name;
        newUser.department = req.body.department;
        newUser.email = req.body.email;
        newUser.phone = req.body.phone;
        newUser.isAdmin = req.body.isAdmin;

        newUser.save(err => {
            if(err){
                res.status(500).json({ message: err});
            }
            res.status(200).json(newUser);
        })
    });

     // '/v1/user/add' - Create 2
     api.get('/addByGet/:cardId&:name&:department&:email&:phone&:isAdmin', (req,res) => {
        let newUser = new User();
        newUser.cardId = req.params.cardId;
        newUser.name = req.params.name;
        newUser.department = req.params.department;
        newUser.email = req.params.email;
        newUser.phone = req.params.phone;
        newUser.isAdmin = req.params.isAdmin;

        newUser.save(err => {
            if(err){
                res.status(500).json({ message: err});
            }
            res.status(200).json(newUser);
        })
    });

    // '/v1/user/' - Read
    api.get('/', (req, res) => {
        User.find({}, (err, users) => {
        if (err) {
            res.status(500).json({ message: err });
        }
        res.status(200).json(users);
        });
    });
    // '/v1/user/:id' - Read 1
    api.get('/:id', (req, res) => {
        User.findById(req.params.id, (err, user) => {
        if (err) {
            res.status(500).json({ message: err });
        }
        res.status(200).json(user);
        });
    });

    // '/v1/user/:cardId' - Read 2
    api.get('/byCard/:cardId', (req, res) => {
         User
            .findOne({ 'cardId': req.params.cardId })
            .exec((err, userData) => {
                if (err) {
                res.status(500).json({ message: err });
                }
                res.status(200).json(userData);
            });
    });
    return api;
}

