import { Router } from 'express'
import httpRequest from 'request'

let api = Router();

    // '/msp/events' - Read
    api.get('/', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        var url = 'https://gateway.eu1.mindsphere.io/api/eventmanagement/v3/events';
        httpRequest.get({
            url: url,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error) {
    
            if (answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body)._embedded.events);
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });
    // '/msp/events/:entityId' - Read
    api.get('/:entityId', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        var url = `https://gateway.eu1.mindsphere.io/api/eventmanagement/v3/events?filter={"entityId": "${req.params.entityId}"}`;
        httpRequest.get({
            url: url,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error) {
      
            if (answer.statusCode == 204 || 200 ) {
                if(JSON.parse(body)._embedded){
                res.status(200).json(JSON.parse(body)._embedded.events);
                }else {
                    res.status(200).json([]);
                }
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });
    // '/msp/events' - Create
    api.post('/', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        var url = 'https://gateway.eu1.mindsphere.io/api/eventmanagement/v3/events';
        var event = req.body;

        httpRequest.post({
            url: url,
            headers: { 'Authorization': 'Bearer ' + req.token, 'Content-Type': 'application/json', 'Accept': '*/*' },
            json: event
        }, function (error, answer, body) {
            if (!error) {
      
            if (answer.statusCode == 204 || 200) {
                res.status(200).json(body);
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });

     // '/msp/events/:eventId' - Update
     api.put('/:eventId', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        var url = `https://gateway.eu1.mindsphere.io/api/eventmanagement/v3/events/${req.params.eventId}`;
        var event = req.body;

        httpRequest.put({
            url: url,
            headers: { 'Authorization': 'Bearer ' + req.token, 'Content-Type': 'application/json', 'Accept': '*/*', 'If-Match' : 0},
            json: event
        }, function (error, answer, body) {
            if (!error) {
      
            if (answer.statusCode == 204 || 200) {
                res.status(200).json(body);
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });

    // '/msp/events/:eventId' - Delete By Id
    api.delete('/:eventId', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        var url = `https://gateway.eu1.mindsphere.io/api/eventmanagement/v3/deleteEventsJobs`;
        var filter = {
            "filter": {
              "typeId": "com.siemens.mindsphere.eventmgmt.event.type.MindSphereStandardEvent",
              "id": req.params.eventId
            }
          }

        httpRequest.post({
            url: url,
            headers: { 'Authorization': 'Bearer ' + req.token, 'Content-Type': 'application/json', 'Accept': '*/*', 'If-Match' : 0},
            json: filter
        }, function (error, answer, body) {
            if (!error) {
      
            if (answer.statusCode == 204 || 200) {
                res.status(200).json(body);
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });

export default api;
