import { Router } from 'express'
import httpRequest from 'request'

let api = Router();

    // '/msp/assets/' - Read
    api.get('/', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: 'https://gateway.eu1.mindsphere.io/api/assetmanagement/v3/assets/',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error) {
      
            if (answer.statusCode == 204 || 200 ) {
                // console.log(JSON.parse(body)._embedded.assets);
                res.status(200).json(JSON.parse(body)._embedded.assets);
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });


export default api;
