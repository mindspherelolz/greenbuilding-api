import { Router } from 'express'
import httpRequest from 'request'

let api = Router();
    // '/msp/timeseries/aggregates/:Id/:sensor' - Read
    api.get('/:Id/:sensor', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        var url = `https://gateway.eu1.mindsphere.io/api/iottimeseries/v3/timeseries/${req.params.Id}/${req.params.sensor}`;
        httpRequest.get({
            url: url,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error) {
    
            if (answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });

    // '/msp/timeseries/aggregates/:Id?sensor=:sensor&start=:start&end=:end&intervalValue=:value&&intervalUnit=:unit&select=:select' - Read
    api.get('/aggregates/:Id/sensor=:sensor&start=:start&end=:end&intervalValue=:value&intervalUnit=:unit&select=:select', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        var url = `https://gateway.eu1.mindsphere.io/api/iottsaggregates/v3/aggregates/${req.params.Id}/${req.params.sensor}?from=${req.params.start}&to=${req.params.end}&intervalValue=${req.params.value}&intervalUnit=${req.params.unit}&select=${req.params.select}`;
        httpRequest.get({
            url: url,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error) {
    
            if (answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });
export default api;
