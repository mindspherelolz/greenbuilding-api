import { Router } from 'express'
import Msg from '../model/message';

export default({ config, db}) => {
    let api = Router();

    // '/v1/noti' - Read 
    api.get('/', (req,res) => {
        Msg.find({}, (err, msgs) => {
            if (err) {
                res.status(500).json({ message: err });
            }
            res.status(200).json(msgs);
         });
    });
    // '/v1/noti/count' - Read 
    api.get('/count', (req,res) => {
        Msg.count({}, (err, count) => {
            if (err) {
                res.status(500).json({ message: err });
            }
            res.status(200).json({count: count});
         });
    });
    // '/v1/noti/add' - Create
    api.post('/add', (req,res) => {
        let newMsg = new Msg();
        newMsg.priority = req.body.priority;
        newMsg.subject = req.body.subject;
        newMsg.body = req.body.body;
        // newMsg.read = false;
        // newMsg.solve = false;

        newMsg.save(err => {
            if(err){
                res.status(500).json({ message: err});
            }
            res.status(200).json(newMsg);
        })
    });
    // '/v1/noti/:id' - Delete
    api.delete('/:id', (req,res) => {

        Msg.remove({
            _id: req.params.id
          }, (err, message) => {
            if (err) {
              res.status(500).json({ message: err });
            }
            res.status(200).json({ message: 'Message Successfully Removed'});
          });
    });
     // '/v1/noti/subject/:subject' - Delete
     api.delete('/subject/:subject', (req,res) => {
        Msg.deleteMany({
            subject: req.params.subject
          }, (err, message) => {
            if (err) {
              res.status(500).json({ message: err });
            }
            res.status(200).json({ message: 'Message Successfully Removed'});
          });
    });

    // '/v1/noti/read/all' - Update 
    api.put('/read/all', (req,res) => {
        Msg.updateMany({"read": false}, {"$set":{"read": true}}, (err, msgs) => {
            if (err) {
                res.status(500).json({ message: err });
            }
            res.status(200).json(msgs);
        });
    });

    // '/v1/noti/read' - Update 
    api.put('/read/:id', (req,res) => {
        Msg.findOneAndUpdate({ _id: req.params.id }, {"read": true}, (err, msg) => {
            if (err) {
                res.status(500).json({ message: err });
            }
            res.status(200).json({ message: 'Message Read!!'});
        });
    });

    // '/v1/noti/solved' - Update 
    api.put('/solve/:id', (req,res) => {
        Msg.findOneAndUpdate({ _id: req.params.id }, {"solved": true}, (err, msg) => {
            if (err) {
                res.status(500).json({ message: err });
            }
            res.status(200).json({ message: 'Message Solved!!'});
        });
    });

    // '/v1/noti/solve/all' - Update 
    api.put('/solve/all', (req,res) => {
        Msg.updateMany({"solved": false}, {"$set":{"solved": true}}, (err, msgs) => {
            if (err) {
                res.status(500).json({ message: err });
            }
            res.status(200).json(msgs);
        });
    });

    return api;
}