import { Router } from 'express'
import httpRequest from 'request'

let api = Router();

    // '/external/meteor/' - Read
    api.get('/', (req, res) => {
        // if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: 'https://opendata.cwb.gov.tw/api/v1/rest/datastore/O-A0003-001?Authorization=CWB-CEDDC52E-2600-4B17-BC4C-5FF70349B8B2&format=JSON',
            headers: { 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error) {
      
            if (answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });
    // '/external/meteor/location=?locationName' - Read
    api.get('/location=?:locationName', (req, res) => {
        // if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: encodeURI(`https://opendata.cwb.gov.tw/api/v1/rest/datastore/O-A0003-001?Authorization=CWB-CEDDC52E-2600-4B17-BC4C-5FF70349B8B2&format=JSON&locationName=${req.params.locationName}`),
            headers: { 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error) {
      
            if (answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body).records);
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });

        // '/external/meteor/taipei' - Read
        api.get('/taipei', (req, res) => {
            // if(!req.token) return res.status(401).send('Acess denied. No token provided.');
            httpRequest.get({
                url: encodeURI('https://opendata.cwb.gov.tw/api/v1/rest/datastore/O-A0003-001?Authorization=CWB-CEDDC52E-2600-4B17-BC4C-5FF70349B8B2&format=JSON&locationName=臺北'),
                headers: { 'content-type': 'application/json' }
            }, function (error, answer, body) {
                if (!error) {
          
                if (answer.statusCode == 204 || 200 ) {
                    res.status(200).json(JSON.parse(body).records);
                }
                else {
                    console.log('Error: ' + error);
                }
                }
                else {
                console.log('Error: ' + error);
                }
            });
        });

        // '/external/meteor/taipei' - Read
        api.get('/siemens', (req, res) => {
            // if(!req.token) return res.status(401).send('Acess denied. No token provided.');
            httpRequest.get({
                url: encodeURI(`https://api.openweathermap.org/data/2.5/weather?q=Taipei,tw&APPID=ce1792b436e4b31103403b2a5f643578&lang=en&units=metric`),
                headers: { 'content-type': 'application/json' }
            }, function (error, answer, body) {
                if (!error) {
          
                if (answer.statusCode == 204 || 200 ) {
                    res.status(200).json(JSON.parse(body));
                }
                else {
                    console.log('Error: ' + error);
                }
                }
                else {
                console.log('Error: ' + error);
                }
            });
        });


export default api;
