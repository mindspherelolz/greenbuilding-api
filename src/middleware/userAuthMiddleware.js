const httpRequest = require('request');
const tokenMid = require('./getToken');
module.exports = function(req, res, next) {
    tokenMid(req,res, function(){
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: 'https://gateway.eu1.mindsphere.io/api/iottimeseries/v3/timeseries/6d4a5dbb9d09452287d5495d2a4d506f/rfid',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error) {
      
            if (answer.statusCode == 204 || 200 ) {
                console.log(JSON.parse(body)[0].user_id);
                next();
            }
            else {
                console.log('Error: ' + error);
            }
            }
            else {
            console.log('Error: ' + error);
            }
        });
    });
}
