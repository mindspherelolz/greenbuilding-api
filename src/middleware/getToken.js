const JWT = require('jwt-simple');
const httpRequest = require('request');
const btoa = require('btoa');
module.exports = function(req, res, next)  {

    var clientId = "sagtwdev-vuetest-1.0.0" || 'localdev'
    var clientSecret = "kVyzMHvQsHZRfVCw2MtEcqvwX7rWh7HN9yPNeTeT7VS" || 'localdev';
    var appName = "vuetest" || 'localdev';
    var appVersion = "1.0.0" || 'localdev';
  
    var stringToEncode = clientId + ':' + clientSecret;
    var base64 = btoa(stringToEncode);
  
    // console.log('stringToEncode:' + stringToEncode);
    // console.log('clientId:' + clientId);
    // console.log('clientSecret:' + clientSecret);
    // console.log('base64:' + base64);
  
    var jsondata = {
        appName: appName,
        appVersion: appVersion,
        hostTenant: 'sagtwdev',
        userTenant: 'sagtwdev'
    };
  
    //   //fetch JWT
    httpRequest.post({
        url: 'https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',
        headers: { 'X-SPACE-AUTH-KEY': 'Basic ' + base64, 'content-type': 'application/json' },
        json: jsondata
    }, function (error, answer, body) {
        if (!error) {
  
        if (answer.statusCode == 200) {
            var response = body;
            // console.log('Response: ' + body);
  
            if (response.access_token) {
              var JWTtoken = response.access_token;
            // console.log('AccessToken: ' + JWTtoken);
            try {
                var decodedJWT = JWT.decode(response.access_token, null, true);
                // console.log('decoded AccessToken: ' + JSON.stringify(decodedJWT));
            } catch (e) {
                // console.log("error occured when decoding JWT", e);
            }
              req.token = JWTtoken;
              next();
            }
        }
        else {
            console.log('Error: ' + error);
        }
        }
        else {
        console.log('Error: ' + error);
        }
    });
}