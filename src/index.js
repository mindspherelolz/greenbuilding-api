import http from 'http'
import express from 'express'
import bodyParser from 'body-parser'
import passport from 'passport'
import socket from 'socket.io'

const LocalStrategy  = require('passport-local').Strategy;

import config from './config'
import routes from './routes'
import msp from './routes/msp'
import external from './routes/external'

//modules
import Message from './model/message'

const defaultPath = '/backend'

let app = express()
// app.use('/backend');
app.server = http.createServer(app);
let io = socket(app.server,{
  path: defaultPath + '/socket',
  origins: '*:*'
});

//CROS
app.use("*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  if (req.method === 'OPTIONS') {
    res.send(200)
  } else {
    next()
  }
});

//middleware
//parse application/json
app.use(bodyParser.json({
    limit: config.bodyLimit
  }));

// API endpoints
//api routes v1
app.use(defaultPath + '/v1', routes);
//api MindSphere
app.use(defaultPath + '/msp', tokenMid,msp);
//api external
app.use(defaultPath + '/external',external);

// app socket
// app.use('/backend/socket', socketRouter)

//api Token 
// app.use('/backend/token', (req,res)=>{
//   res.json({message: req.headers.authorization});
// });
import tokenMid from './middleware/userAuthMiddleware'
app.get('/backend/token', tokenMid, (req, res) => {
  res.json({ token: req.token })
});

// Base URL test endpoint to see if API is running
app.get('/backend/', (req, res) => {
  res.json({ message: 'Smart Building API is ALIVE!' });
});

/*||||||||||||||||SOCKET|||||||||||||||||||||||*/
require('./socket/notification')(io);
// //Listen for connection
// let notiSocket = io.of('/noti');
// notiSocket.on('connection', function(client){
//   console.log('a user connected');
//   //Listens for a new message
//   client.on('newMessage', function(priority, subject, body) {
//     //Create Message
//     let newMessage = new Message({
//     priority: priority,
//     subject: subject,
//     body: body
//   });
//     //Save it to database
//     newMessage.save(function(err, message){
//       //Send message to those connected in the noti
//       console.log('new message created');
//       notiSocket.emit("messageCreated", message);
//     });
//   });

//   //Listens for a message deleted
//   client.on('deleteMessageById', function(Id) {
//     //Delete Msg from database
//     Message.remove({
//       _id: Id
//     }, (err, message) => {
//       if (err) {
//         console.log(err);
//       }
//       console.log('A message deleted by id');
//       notiSocket.emit("aMessageDeleteById", message)
//     });
//   });

//   //Listens for a message read
//   client.on('readMessage', function(Id) {
//     //Read Msg and save status to database
//     Message.findOneAndUpdate({ _id: Id }, {"read": true}, (err, message) => {
//       if (err) {
//           console.log(err);
//       }
//       console.log('A message read by an user');
//       notiSocket.emit("aMessageRead", message)
//   });
//   });

//   //Listens for a message solved
//   client.on('solvedMessage', function(Id) {
//     //Solved Msg and save status to database
//     Message.findOneAndUpdate({ _id: Id }, {"solved": true}, (err, message) => {
//       if (err) {
//           console.log(err);
//       }
//       console.log('A message solved by an user');
//       notiSocket.emit("aMessageSolved", message)
//   });
//   });
// });

/*||||||||||||||||||||END SOCKETS||||||||||||||||||*/

// Listening  port
app.server.listen(config.port);

console.log(`Started on port ${app.server.address().port}`);
module.exports = {
  app,
  io
}
