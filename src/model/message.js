import mongoose from 'mongoose'
const Schema = mongoose.Schema;

const msgSchema = new Schema({
    priority: {
        type: Number,
        required: true
    },
    subject: {
        type: String, 
        required: true
    },
    body: {
        type: String,
        required: true
    },
    read: {
        type: Boolean,
        required: true,
        default: false
    },
    timestamp : { 
        type : Date,
        default: Date.now 
    },
    solved: {
        type: Boolean,
        required: true,
        default: false
    },
    from: String, default: "",
    recipient: String, default: ""
});

module.exports = mongoose.model('Message', msgSchema);