import mongoose from 'mongoose'
const Schema = mongoose.Schema;
import passportLocalMongoose from 'passport-local-mongoose';

const userSchema = new Schema({
    cardId: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    department: String, default: "",
    email: String, default: "",
    phone: String, default: "",
    isAdmin: Boolean, default: false
});

// userSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('User', userSchema);
