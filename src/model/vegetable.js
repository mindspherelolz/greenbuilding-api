import mongoose from 'mongoose'
const Schema = mongoose.Schema;

const msgSchema = new Schema({
    vegId: {
        type: Number,
        required: true
    },
    name: {
        type: String, 
        required: true
    },
    quantities: {
        type: Number,
        required: true
    },
    available: {
        type: Boolean,
        required: true,
        default: false
    },
    timestamp : { 
        type : Date,
        default: Date.now 
    },
    solved: {
        type: Boolean,
        required: true,
        default: false
    },
    from: String, default: "",
    recipient: String, default: ""
});

module.exports = mongoose.model('Message', msgSchema);